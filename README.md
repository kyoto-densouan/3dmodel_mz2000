# README #

1/3スケールのSHARP MZ-2000風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- シャープ

## 発売時期
- 1982年

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/MZ-2000)
- [懐かしのホビーパソコン紹介](https://twitter.com/i/events/797650216383938560)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_mz2000/raw/baa22fe92be323de4ba4c29bb74e67ea01eefa7e/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_mz2000/raw/baa22fe92be323de4ba4c29bb74e67ea01eefa7e/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_mz2000/raw/baa22fe92be323de4ba4c29bb74e67ea01eefa7e/ExampleImage.jpg)
